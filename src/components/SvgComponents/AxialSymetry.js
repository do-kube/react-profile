import React from "react";

export default function AxialSymetry(props) {
    return React.Children.map(props.children, kid => (
        <React.Fragment>
            {kid}
            <g style={{ transform: 'scale(1, -1) rotate(180deg)' }}>{kid}</g>
        </React.Fragment>
    ));
}
