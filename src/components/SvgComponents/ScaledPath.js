import React from "react";

export default function ScaledPath({ x = 0, y = 0, duration, delay, style, ...rest }) {
    return (
        <path
            style={{
                transform: `scale(${x}, ${y})`,
                animation: `scale ${duration}s ease-out forwards ${delay}s`,
                ...style,
            }}
            {...rest}
        />
    )
}
