import React from "react";

export default function RadialSymetry({ children, offset = 0, angle, repeat }) {
    return Array.from({ length: repeat }, (_, index) =>
        React.Children.map(children, kid => (
            <g style={{ transform: `rotate(${offset + angle * index}deg)` }}>{kid}</g>
        ))
    );
}
