import React from "react";

function Scene(props) {
    return (
        <svg
            viewBox="0 0 480 300"
            xmlns="http://www.w3.org/2000/svg"
            strokeWidth={5}
            fill="none"
            stroke="black"
        >
            {props.children}
        </svg>
    );
}
export default Scene;
