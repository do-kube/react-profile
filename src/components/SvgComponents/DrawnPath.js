import React from "react";

export default function DrawnPath({ pathLength, duration, delay, style, ...rest }) {
    return (
        <path
            style={{
                strokeDashoffset: pathLength,
                strokeDasharray: pathLength,
                animation: `draw ${duration}s ease-out forwards ${delay}s`,
                ...style,
            }}
            {...rest}
        />
    );
}
