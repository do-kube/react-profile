import React from 'react';
import {
    Navbar,

} from 'react-bootstrap';
import Logo from "./logo";
import './topbar.css';

function Topbar() {
    // const [isOpen, setIsOpen] = useState(false);
    // const toggle = () => setIsOpen(!isOpen);

    return (
            <Navbar className={"topbar"} dark="true" expand="md">
                <Navbar.Brand href="/">
                    <Logo />
                    Ian Leedham
                </Navbar.Brand>
{/*                <Navbar.Toggle onClick={toggle} />
                <Navbar.Collapse isOpen={isOpen} navbar>
                    <Nav className="ml-auto" navbar>
                            <Nav.Link href="/section/">section</Nav.Link>
                            <Nav.Link href="/section/">section</Nav.Link>
                    </Nav>
                </Navbar.Collapse>*/}
            </Navbar>
    );
}

export default Topbar;
