import React, {Component} from 'react';
import Scene from "../SvgComponents/Scene";
import AxialSymetry from "../SvgComponents/AxialSymetry";
import DrawnPath from "../SvgComponents/DrawnPath";
import RadialSymetry from "../SvgComponents/RadialSymetry";
import ScaledPath from "../SvgComponents/ScaledPath";
import './style.css'

class Header extends Component {

    constructor(props) {
        super(props);
        this.state = {
            width:  800,
            height: 182
        }
    }
    RAYS = 14;
    ANGLE = 180 / (this.RAYS - 1);

    /**
     * Calculate & Update state of new dimensions
     */
    updateDimensions() {
        if(window.innerWidth < 500) {
            this.setState({ width: 450, height: 102 });
        } else {
            let update_width  = window.innerWidth-100;
            let update_height = Math.round(update_width/4.4);
            this.setState({ width: update_width, height: update_height });
        }
    }

    /**
     * Add event listener
     */
    componentDidMount() {
        this.updateDimensions();
        window.addEventListener("resize", this.updateDimensions.bind(this));
    }

    /**
     * Remove event listener
     */
    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions.bind(this));
    }

    render () {
        return (
                    <div className={"col-sm-8 col-sm-offset-2"}>
                        <img src={require('../../linkedin.jpeg')}
                             style={{
                                 position: 'absolute',
                                 top: '50%',
                                 left: '50%',
                                 transform: 'translate(-50%, -50%)',
                                 clipPath: `circle(${ this.state.width / 16}px at ${ this.state.width / 16}px ${ this.state.width / 16}px)`,
                                 width: this.state.width / 8
                             }}
                             alt={""}
                        />
                        <Scene>
                            <AxialSymetry>
                                <DrawnPath
                                    id="Mountain__Large"
                                    d="M144.74 244l37.38-54 37.39 54H48"
                                    pathLength={302.9}
                                    duration={1.2}
                                    delay={-0.1}
                                />
                                <DrawnPath
                                    id="Mountain__Small"
                                    d="M123 244l24.92-36 24.93 36"
                                    pathLength={87.6}
                                    duration={0.6}
                                    delay={0.8}
                                />
                            </AxialSymetry>
                            <RadialSymetry repeat={this.RAYS} angle={this.ANGLE}>
                                <DrawnPath
                                    id="Ray__Main"
                                    d="M80 244H48"
                                    pathLength={50}
                                    duration={0.8}
                                    delay={1.2}
                                />
                                <DrawnPath
                                    id="Ray__Tip"
                                    d="M41 244h-9"
                                    pathLength={9}
                                    duration={0.1}
                                    delay={2}
                                />
                            </RadialSymetry>
                            <ScaledPath
                                d="M116 258h248"
                                id="Baseline"
                                // only scale the line on the X axis
                                y={1}
                                duration={0.6}
                                delay={1.7}
                            />
                        </Scene>
                    </div>
        );
    }
}

export default Header;
