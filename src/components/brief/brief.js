import React  from 'react';
// get our fontawesome imports
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEnvelope } from '@fortawesome/free-solid-svg-icons'
import { faGithub, faLinkedin } from '@fortawesome/free-brands-svg-icons'

function Brief() {

    return (
    <div className={'text-center'}>
        <h1>Hi, I'm Ian Leedham</h1>

        <p>I'm a full stack developer working at Ehsdata</p>
        <p>I primarily specialise in dotnet, typescript, MsSql and azure devops</p>
        <div className={"row "}>
            <div className={'col text-center'}>
                <a href={'mailto:ian.leedham@hotmail.com'} target={'_'} className={'m-1'}><FontAwesomeIcon icon={faEnvelope} size="2x" /></a>
                <a href={'https://www.linkedin.com/in/ian-leedham/'} target={'_'} className={'m-1'}><FontAwesomeIcon icon={faLinkedin} size="2x" /></a>
                <a href={'https://github.com/ianleedham'} target={'_'} className={'m-1'}><FontAwesomeIcon icon={faGithub} size="2x" /></a>
            </div>
        </div>
    </div>
    );
}

export default Brief;
