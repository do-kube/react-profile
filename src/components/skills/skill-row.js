import React  from 'react';
import ProgressBar from 'react-bootstrap/ProgressBar';

function SkillRow(props) {

    return (
            <>
                <div className={'col-sm-6'} style={{ fontSize: '1.5rem', }}>{props.skill}</div>
                <div  className={'col-sm-6'}  style={{ padding: '0.5rem', }}>
                    <ProgressBar animated now={props.progress * 10} label={props.progress + `/10`}
                                 style={{ height: '1.5rem', fontSize: '1rem' }}/>
                </div>
            </>
    );
}

export default SkillRow;
