import React  from 'react';
import SkillRow from "./skill-row";

function SkillsColumn(props) {

    return (
        <div className={'col-sm-11'}>
            <div className={'row text-center'}>
                <div className={'col'}>
                    <h3>{props.columnTitle}</h3>
                </div>
            </div>
            {props.skills.map( skill => {
                return <div key={skill.name} className={'row'}>
                    <SkillRow  skill={skill.name} progress={skill.level}/>
                </div>
            })}

        </div>
    );
}

export default SkillsColumn;
