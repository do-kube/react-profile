import React  from 'react';
import SkillsColumn from "./skills-column";

function Skills() {

    const skills = {
        languages: [
            {
                name: "mssql",
                level: 5,
            },
            {
                name: 'type script',
                level: 8
            },
            {
                name: "C#",
                level: 8
            },
            {
                name: "Dotnet core",
                level: 8
            },
            {
                name: "Dotnet framework",
                level: 6
            }
        ],
        tools: [
            {
                name: "visual studio",
                level: 8
            },
            {
                name: "mssql toolbox",
                level: 8
            },
            {
                name: 'git',
                level: 9
            },
            {
                name: 'docker',
                level: 6
            }
        ],
        concepts:
        [
            {
                name: 'SOLID',
                level: 6
            },
            {
                name: 'C# Design priciples',
                level: 5
            }
        ]
    }

    return (
        <div className={'col-sm-11'}>
            <div className={'row'}><h1 > Skills </h1></div>
            <div className={'row'}>
                <div className={'col-sm-4'}>
                    <SkillsColumn columnTitle={"Languages and Frameworks"} skills={skills.languages} />
                </div>
                <div className={'col-sm-4'}>
                    <SkillsColumn columnTitle={"Tools"} skills={skills.tools}/>
                </div>
                <div className={'col-sm-4'}>
                    <SkillsColumn columnTitle={"Concepts"} skills={skills.concepts}/>
                </div>
            </div>
        </div>
    );
}

export default Skills;
