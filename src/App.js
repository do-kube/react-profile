import React from 'react';
import './App.css';
import Topbar from "./components/topbar/topbar";
import "bootstrap/dist/css/bootstrap.min.css"
import Header from "./components/header/header";
import Brief from "./components/brief/brief";
import Skills from "./components/skills/skills";

function App() {
  return (

      <div className="App">
        <Topbar className={'row '} />
        <div className={"conatiner"}>
            <div className={"row justify-content-around"}><Header /></div>
            <div className={"row justify-content-around"}><Brief /></div>
            <div className={"row justify-content-around"}><Skills /></div>
        </div>
    </div>
  );
}

export default App;
